# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Invoice Credit of PARTNER on POS screen',
    'version': '1.1',
    'website': '',
    
    "price": 12.00,
    "currency": 'USD',
    'license': 'AGPL-3',
    'author': 'Tb25',
    'email': 'torbatj79@gmail.com',
    
    'category': 'POS',
    'sequence': 1,
    'summary': 'See invoice credit of partner in POS. Credit on pos. Partner credit. Credit of partner. Invoice credit. Partner invoice credit. Partner invoice receive. Partner credit receivable. POS credit. POS partner credit. POS invoice credit.',
    'depends': [
        'base',
        'point_of_sale',
    ],
    'description': "You can see Invoice credits of partner on the POS screen",
    'qweb': ['static/src/xml/pos.xml'],
    'images': [
        'static/src/img/banner.png'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
